# ProgressView
![image](./images/progress.gif)

##### 项目介绍


- 功能：实现一个进度视图，目前实现了带数字进度的水平进度条以及圆形进度条，圆形进度条包括三种风格：普通环形进度，内部垂直填充进度以及内部环形填充进度。


##### 安装教程

方式一

1. 在需要使用的模块的build.gradle中引入

```
dependencies {
    compile project(path: ':progress')
}
```

在sdk5，DevEco Studio2.2 bate1下项目可直接运行

方式二

在project的build.gradle中添加mavenCentral()的引用

```
repositories {   
 	...   
 	mavenCentral()   
	 ...           
 }
```

在entry的build.gradle中添加依赖

```
dependencies { 
... 
implementation 'com.gitee.archermind-ti:progress:1.0.0-beta' 

... 
}
```

##### 使用说明
1. 在布局文件layout.xml中添加progressview控件，设置对应属性
```xml

   <com.white.progressview.CircleProgressView
                   ohos:id="$+id:circle_progress_normal"
                   ohos:height="160vp"
                   ohos:width="0vp"
                   ohos:padding="10vp"
                   ohos:progress="35"
                   app:progressNormalSize="8vp"
                   app:progressReachSize="8vp"
                   app:progressStyle="0"
                   app:radius="28vp"
                   ohos:weight="1"
           />

    <com.white.progressview.HorizontalProgressView
            ohos:height="40vp"
            ohos:width="match_parent"
            ohos:padding="10vp"
            ohos:progress="28"
            app:progressTextVisible="true"
    />

```
2. 也可以在AbilitySlice文件中通过代码设置属性
 
```
horizontalProgressView.setXXXX();

horizontalProgressView.setTextVisible(false);
horizontalProgressView.setReachBarSize(4);
horizontalProgressView.setProgressPosition(HorizontalProgressView.TOP);
...
```
设置动画时间
```
HorizontalProgressView horizontalProgressView = (HorizontalProgressView) findComponentById(ResourceTable.Id_progress100);
// set progress 100 with anim in 2000ms 
horizontalProgressView.setProgressInTime(100,2000);
// set progress from 20 to 100 with anim in 2000ms 
horizontalProgressView.setProgressInTime(0,100,2000);
// reset current progress with anim in 2000ms 
horizontalProgressView.runProgressAnim(2000);

CircleProgressView circleProgressView = (CircleProgressView) findViefindComponentByIdwById(ResourceTable.Id_circle_progress_normal);
...
// same as HorizontalProgressView
circleProgressView.setProgressInTime(100,2000);
    
```
##### 自定义属性


#### HorizontalProgressView

属性名 | 属性说明
:---:|:---:
progressNormalColor | 设置正常状态下进度条颜色
progressReachColor | 设置已到达的进度条颜色
progressTextColor | 设置进度文本颜色
progressTextSize | 设置进度文本大小
progressTextOffset | 设置进度文本与进度条的边距
progressNormalSize | 设置正常状态下进度条大小
progressReachSize | 设置已到达的进度条大小
progressTextPosition | 设置进度文本位置(CENTER/BOTTOM/TOP)(居中/底部/顶部)
progressTextVisible | 设置是否显示进度文本
progressTextSkewX | 设置进度文本倾斜角度(斜体)
progressTextPrefix | 设置进度文本前缀
progressTextSuffix | 设置进度文本后缀(默认是%)
#### CircleProgressView

属性名 | 属性说明
:---:|:---:
progressNormalColor | 设置正常状态下进度条颜色
progressReachColor | 设置已到达的进度条颜色
progressTextColor | 设置进度文本颜色
progressTextSize | 设置进度文本大小
progressTextOffset | 设置进度文本与进度条的边距
progressNormalSize | 设置正常状态下进度条大小
progressReachSize | 设置已到达的进度条大小
radius | 设置圆形半径
progressTextVisible | 设置是否显示进度文本
progressTextSkewX | 设置进度文本倾斜角度(斜体)
progressTextPrefix | 设置进度文本前缀
progressTextSuffix | 设置进度文本后缀(默认是%)
progressStartArc | 设置进度值起始角度
progressStyle | 设置圆形进度样式(Normal/FillInner/FillInnerArc)(普通环形/内部垂直填充/内部环形填充)
reachCapRound | 设置进度边界是否使用圆角(使用Normal样式时有用)
innerBackgroundColor | 设置内部背景颜色(使用Normal样式时有用)
innerProgressColor | 设置内部进度颜色(使用FillInner样式时有用)
innerPadding | 设置内部圆与外部圆的间距(使用FillInnerArc样式时有用)
outerColor | 设置外部圆环颜色(使用FillInnerArc样式时有用)
outerSize | 设置外部圆环大小(使用FillInnerArc样式时有用)



#### 版本迭代

- v1.0.0-beta

#### 基线release版本

commit 82ab1fdde27de478301aaede0c5fddf79751896f

#### 版权和许可信息
LICENSE
Copyright 2017 Wh1te

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.