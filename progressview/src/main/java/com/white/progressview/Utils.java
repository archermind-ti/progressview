package com.white.progressview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Optional;

/**
 * Attr utils.
 */
public class Utils {
    /**
     * * The constant TAG_LOG.
     */
    private static final String TAG_LOG = "systembartint";

    /**
     * * The constant DOMAIN_ID.
     */
    private static final int DOMAIN_ID = 0;

    /**
     * * The constant LABEL_LOG.
     */
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, DOMAIN_ID, TAG_LOG);

    /**
     * * The constant LOG_FORMAT.
     */
    private static final String LOG_FORMAT = "%{public}s: %{public}s";

    /**
     * Get int from attr int.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the int
     */
    public static int getIntFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get float from attr float.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the float
     */
    public static float getFloatFromAttr(AttrSet attrs, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get boolean from attr boolean.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the boolean
     */
    public static boolean getBooleanFromAttr(AttrSet attrs, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get long from attr long.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the long
     */
    public static long getLongFromAttr(AttrSet attrs, String name, long defaultValue) {
        long value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get color from attr int.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the int
     */
    public static int getColorFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get element from attr element.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the element
     */
    public static Element getElementFromAttr(AttrSet attrs, String name, Element defaultValue) {
        Element value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getElement();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get dimension from attr int.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the int
     */
    public static int getDimensionFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get string from attr string.
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the string
     */
    public static String getStringFromAttr(AttrSet attrs, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, Utils.class.getName(), e.getMessage());
        }
        return value;
    }

    public static int dp2px(Context context, int dpVal) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        final float scale = displayAttributes.densityPixels;
        return (int) (dpVal * scale + 0.5f);
    }

    /**
     * Sp 2 px int.
     *
     * @param context context
     * @param spVal   sp val
     * @return the int
     */
    public static int sp2px(Context context, int spVal) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        final float fontScale = displayAttributes.scalDensity;
        return (int) (spVal * fontScale + 0.5f);
    }
}
