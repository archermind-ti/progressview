package com.white.progressview;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * HorizontalProgressView.
 */
public class HorizontalProgressView extends ProgressBar {
    /**
     * * The constant TOP
     */
    public static final int TOP = 1;
    /**
     * * The constant CENTRE
     */
    public static final int CENTRE = 0;
    /**
     * * The constant BOTTOM
     */
    public static final int BOTTOM = -1;

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSNORMALCOLOR = "progressNormalColor";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSREACHCOLOR = "progressReachColor";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTCOLOR = "progressTextColor";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTSIZE = "progressTextSize";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTOFFSET = "progressTextOffset";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSNORMALSIZE = "progressNormalSize";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSREACHSIZE = "progressReachSize";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTPOSITION = "progressTextPosition";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTVISIBLE = "progressTextVisible";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTSKEWX = "progressTextSkewX";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTPREFIX = "progressTextPrefix";

    public static final String HORIZONTALPROGRESSVIEW_PROGRESSTEXTSUFFIX = "progressTextSuffix";


    private int mNormalBarSize = Utils.dp2px(getContext(), 2);

    private int mNormalBarColor = Color.getIntColor("#FFD3D6DA");

    private int mReachBarSize = Utils.dp2px(getContext(), 2);

    private int mReachBarColor = Color.getIntColor("#108ee9");

    private int mTextSize = Utils.sp2px(getContext(), 14);

    private int mTextColor = Color.getIntColor("#108ee9");

    private int mTextOffset = Utils.dp2px(getContext(), 6);

    private int mBackgroundColor = Color.WHITE.getValue();

    private int mProgressPosition = CENTRE;

    private boolean mTextVisible = true;

    private String mTextPrefix = "";

    private String mTextSuffix = "%";

    private Paint mTextPaint;

    private Paint mNormalPaint;

    private Paint mReachPaint;
    /**
     * 经过测量后得到的需要绘制的总宽度
     */
    private int mDrawWidth;
    /**
     * Upgrade SDK 5 to adapt to the onRefresh and onDraw sequences.
     */
    private boolean mIsOnRefreshed = false;

    /**
     * Horizontal progress view.
     *
     * @param context context
     */
    public HorizontalProgressView(Context context) {
        this(context, null);
    }


    /**
     * Horizontal progress view.
     *
     * @param context context
     * @param attrs   attrs
     */
    public HorizontalProgressView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    /**
     * Horizontal progress view.
     *
     * @param context      context
     * @param attrs        attrs
     * @param defStyleAttr def style attr
     */
    public HorizontalProgressView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        obtainAttributes(attrs);
        // 初始化画笔
        initPaint();
        setLayoutRefreshedListener(new LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                refreshed(component);
            }
        });
        addDrawTask(new DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                draw(component,canvas);
            }
        });
    }

    /**
     * On draw.
     *
     * @param component component
     * @param canvas    canvas
     */
    private void draw(Component component, Canvas canvas) {
        if (!mIsOnRefreshed) {
            refreshed(component);
            mIsOnRefreshed = true;
        }
        canvas.save();
        canvas.drawColor(mBackgroundColor, Canvas.PorterDuffMode.SRC);
        drawHorizontalProgressView(canvas);
        canvas.restore();
    }

    /**
     * On refreshed.
     *
     * @param component component
     */
    private void refreshed(Component component) {
        // 实际绘制宽度
        mDrawWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        Element backgroundElement = getBackgroundElement();
        if (backgroundElement != null) {
            if (backgroundElement instanceof ShapeElement) {
                RgbColor[] rgbColors = ((ShapeElement) backgroundElement).getRgbColors();
                mBackgroundColor = rgbColors[0].asArgbInt();
            }
        }
    }

    /**
     * Init paint.
     */
    protected void initPaint() {
        mTextPaint = new Paint();
        mTextPaint.setColor(new Color(mTextColor));
        mTextPaint.setStyle(Paint.Style.FILL_STYLE);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setAntiAlias(true); // 抗锯齿

        mNormalPaint = new Paint();
        mNormalPaint.setColor(new Color(mNormalBarColor));
        mNormalPaint.setStyle(Paint.Style.FILL_STYLE);
        mNormalPaint.setAntiAlias(true);
        mNormalPaint.setStrokeWidth(mNormalBarSize);

        mReachPaint = new Paint();
        mReachPaint.setColor(new Color(mReachBarColor));
        mReachPaint.setStyle(Paint.Style.FILL_STYLE);
        mReachPaint.setAntiAlias(true);
        mReachPaint.setStrokeWidth(mReachBarSize);
    }

    /**
     * Set progress value.
     *
     * @param progress progress
     */
    @Override
    public void setProgressValue(int progress) {
        super.setProgressValue(progress);
        invalidate();
    }

    /**
     * 获取自定义属性值.
     *
     * @param attrs attrs
     */
    protected void obtainAttributes(AttrSet attrs) {
        mNormalBarSize = Utils.getDimensionFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSNORMALSIZE, mNormalBarSize);
        mNormalBarColor = Utils.getColorFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSNORMALCOLOR, mNormalBarColor);
        mReachBarSize = Utils.getDimensionFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSREACHSIZE, mReachBarSize);
        mReachBarColor = Utils.getColorFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSREACHCOLOR, mReachBarColor);
        mTextSize = Utils.getDimensionFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTSIZE, mTextSize);
        mTextColor = Utils.getColorFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTCOLOR, mTextColor);
        mTextSuffix = Utils.getStringFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTSUFFIX, mTextSuffix);
        mTextPrefix = Utils.getStringFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTPREFIX, mTextPrefix);
        mTextOffset = Utils.getDimensionFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTOFFSET, mTextOffset);
        mProgressPosition = Utils.getIntFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTPOSITION, mProgressPosition);
        mTextVisible = Utils.getBooleanFromAttr(attrs, HORIZONTALPROGRESSVIEW_PROGRESSTEXTVISIBLE, mTextVisible);
    }

    /**
     * Draw horizontal progress view.
     *
     * @param canvas canvas
     */
    private void drawHorizontalProgressView(Canvas canvas) {
        canvas.translate(getPaddingLeft(), getHeight() / (float)2);

        boolean needDrawUnReachArea = true; // 是否需要绘制未到达进度
        float textWidth = 0;
        String text = mTextPrefix + getProgress() + mTextSuffix;
        if (mTextVisible) {
            textWidth = mTextPaint.measureText(text);
        } else {
            mTextOffset = 0;
        }
        float textHeight = (mTextPaint.descent() + mTextPaint.ascent()) / (float)2;
        float radio = getProgress() * 1.0f / getMax();
        float progressPosX = (int) (mDrawWidth - textWidth) * (radio);

        if (progressPosX + textWidth >= mDrawWidth) {
            progressPosX = mDrawWidth - textWidth;
            needDrawUnReachArea = false;
        }

        // 绘制已到达进度
        float endX = progressPosX - mTextOffset / (float)2;
        if (endX > 0) {
            canvas.drawLine(new Point(0, 0), new Point(endX, 0), mReachPaint);
        }

        // 绘制未到达进度
        if (needDrawUnReachArea) {
            float start = progressPosX + mTextOffset / (float)2 + textWidth;
            canvas.drawLine(new Point(start, 0), new Point(mDrawWidth, 0), mNormalPaint);
        }

        // 绘制字体
        if (!mTextVisible) {
            return;
        }
        switch (mProgressPosition) {
            case BOTTOM: // BOTTOM
                canvas.drawText(mTextPaint, text, progressPosX, -textHeight * 2 + mTextOffset);
                break;
            case TOP: // TOP
                canvas.drawText(mTextPaint, text, progressPosX, 0 - mTextOffset);
                break;
            default: // CENTER
                canvas.drawText(mTextPaint, text, progressPosX, -textHeight);
                break;
        }
    }

    /**
     * Get normal bar size int.
     *
     * @return the int
     */
    public int getNormalBarSize() {
        return mNormalBarSize;
    }

    /**
     * Set normal bar size.
     *
     * @param normalBarSize normal bar size
     */
    public void setNormalBarSize(int normalBarSize) {
        mNormalBarSize = Utils.dp2px(getContext(), normalBarSize);
        invalidate();
    }

    /**
     * Get normal bar color int.
     *
     * @return the int
     */
    public int getNormalBarColor() {
        return mNormalBarColor;
    }

    /**
     * Set normal bar color.
     *
     * @param normalBarColor normal bar color
     */
    public void setNormalBarColor(int normalBarColor) {
        mNormalBarColor = normalBarColor;
        invalidate();
    }

    /**
     * Get reach bar size int.
     *
     * @return the int
     */
    public int getReachBarSize() {
        return mReachBarSize;
    }

    /**
     * Set reach bar size.
     *
     * @param reachBarSize reach bar size
     */
    public void setReachBarSize(int reachBarSize) {
        mReachBarSize = Utils.dp2px(getContext(), reachBarSize);
        invalidate();
    }

    /**
     * Get reach bar color int.
     *
     * @return the int
     */
    public int getReachBarColor() {
        return mReachBarColor;
    }

    /**
     * Set reach bar color.
     *
     * @param reachBarColor reach bar color
     */
    public void setReachBarColor(int reachBarColor) {
        mReachBarColor = reachBarColor;
        invalidate();
    }

    /**
     * Get text size int.
     *
     * @return the int
     */
    public int getTextSize() {
        return mTextSize;
    }

    /**
     * Set text size.
     *
     * @param textSize text size
     */
    public void setTextSize(int textSize) {
        mTextSize = Utils.sp2px(getContext(), textSize);
        invalidate();
    }

    /**
     * Get text color int.
     *
     * @return the int
     */
    public int getTextColor() {
        return mTextColor;
    }

    /**
     * Set text color.
     *
     * @param textColor text color
     */
    public void setTextColor(int textColor) {
        mTextColor = textColor;
        invalidate();
    }

    /**
     * Get text offset int.
     *
     * @return the int
     */
    public int getTextOffset() {
        return mTextOffset;
    }

    /**
     * Set text offset.
     *
     * @param textOffset text offset
     */
    public void setTextOffset(int textOffset) {
        mTextOffset = Utils.dp2px(getContext(), textOffset);
        invalidate();
    }

    /**
     * Get progress position int.
     *
     * @return the int
     */
    @Position
    public int getProgressPosition() {
        return mProgressPosition;
    }

    /**
     * Set progress position.
     *
     * @param progressPosition progress position
     */
    public void setProgressPosition(@Position int progressPosition) {
        if (progressPosition > 1 || progressPosition < -1) {
            mProgressPosition = 0;
        } else {
            mProgressPosition = progressPosition;
        }
        invalidate();
    }

    /**
     * Is text visible boolean.
     *
     * @return the boolean
     */
    public boolean isTextVisible() {
        return mTextVisible;
    }

    /**
     * Set text visible.
     *
     * @param textVisible text visible
     */
    public void setTextVisible(boolean textVisible) {
        mTextVisible = textVisible;
        invalidate();
    }

    /**
     * Get text prefix string.
     *
     * @return the string
     */
    public String getTextPrefix() {
        return mTextPrefix;
    }

    /**
     * Set text prefix.
     *
     * @param textPrefix text prefix
     */
    public void setTextPrefix(String textPrefix) {
        mTextPrefix = textPrefix;
        invalidate();
    }

    /**
     * Get text suffix string.
     *
     * @return the string
     */
    public String getTextSuffix() {
        return mTextSuffix;
    }

    /**
     * Set text suffix.
     *
     * @param textSuffix text suffix
     */
    public void setTextSuffix(String textSuffix) {
        mTextSuffix = textSuffix;
        invalidate();
    }

    /**
     * Run progress anim.
     *
     * @param duration duration
     */
    public void runProgressAnim(long duration) {
        setProgressInTime(0, duration);
    }

    /**
     * Set progress in time.
     *
     * @param progress 进度值
     * @param duration 动画播放时间
     */
    public void setProgressInTime(final int progress, final long duration) {
        setProgressInTime(progress, getProgress(), duration);
    }

    /**
     * Set progress in time.
     *
     * @param startProgress 起始进度
     * @param progress      进度值
     * @param duration      动画播放时间
     */
    public void setProgressInTime(int startProgress, final int progress, final long duration) {
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(startProgress, progress);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setProgressValue((int) value);
            }
        });
        valueAnimator.setInterpolator(Animator.CurveType.ACCELERATE_DECELERATE);
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    /**
     * Invalidate.
     */
    @Override
    public void invalidate() {
        initPaint();
        super.invalidate();
    }

    /**
     * Position @IntDef({TOP, CENTRE, BOTTOM})
     */
    @Retention(RetentionPolicy.SOURCE)
    public @interface Position {

    }
}
