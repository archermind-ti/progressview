package com.white.progress.slice;

import com.white.progress.ResourceTable;
import com.white.progressview.CircleProgressView;
import com.white.progressview.HorizontalProgressView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

/**
 * Demo ability slice
 */
public class DemoAbilitySlice extends AbilitySlice {
    /**
     * The constant M progress view 20
     */
    private HorizontalProgressView mProgressView20;
    /**
     * The constant M progress view 40
     */
    private HorizontalProgressView mProgressView40;
    /**
     * The constant M progress view 60
     */
    private HorizontalProgressView mProgressView60;
    /**
     * The constant M progress view 80
     */
    private HorizontalProgressView mProgressView80;
    /**
     * The constant M progress view 100
     */
    private HorizontalProgressView mProgressView100;

    /**
     * The constant M circle progress view normal
     */
    private CircleProgressView mCircleProgressViewNormal;
    /**
     * The constant M circle progress view fill in
     */
    private CircleProgressView mCircleProgressViewFillIn;
    /**
     * The constant M circle progress view fill in arc
     */
    private CircleProgressView mCircleProgressViewFillInArc;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_demo);
        mProgressView20 = findView(ResourceTable.Id_progress20);
        mProgressView40 = findView(ResourceTable.Id_progress40);
        mProgressView60 = findView(ResourceTable.Id_progress60);
        mProgressView80 = findView(ResourceTable.Id_progress80);
        mProgressView100 = findView(ResourceTable.Id_progress100);
        mProgressView20.setProgressPosition(HorizontalProgressView.BOTTOM);
        mProgressView80.setProgressPosition(HorizontalProgressView.TOP);


        mCircleProgressViewNormal = findView(ResourceTable.Id_circle_progress_normal);
        mCircleProgressViewFillIn = findView(ResourceTable.Id_circle_progress_fill_in);
        mCircleProgressViewFillInArc = findView(ResourceTable.Id_circle_progress_fill_in_arc);
        findComponentById(ResourceTable.Id_btn_start).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                startWithAnim();
            }
        });
    }

    /**
     * Start with anim
     */
    public void startWithAnim() {
        mProgressView20.runProgressAnim(1000);
        mProgressView40.runProgressAnim(2000);
        mProgressView60.runProgressAnim(3000);
        mProgressView80.runProgressAnim(4000);
        mProgressView100.runProgressAnim(5000);

        mCircleProgressViewNormal.runProgressAnim(1000);
        mCircleProgressViewFillIn.runProgressAnim(2000);
        mCircleProgressViewFillInArc.runProgressAnim(3000);
    }


    /**
     * t
     *
     * @param <Type> parameter
     * @param id     id
     * @return the t
     */
    private <Type extends Component> Type findView(int id) {
        return (Type) findComponentById(id);
    }


}
