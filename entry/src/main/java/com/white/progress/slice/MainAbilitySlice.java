package com.white.progress.slice;

import com.white.progress.ResourceTable;
import com.white.progressview.CircleProgressView;
import com.white.progressview.HorizontalProgressView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.utils.Color;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Main ability slice.
 */
public class MainAbilitySlice extends AbilitySlice implements Slider.ValueChangedListener {
    /**
     * The constant M seek bar outer
     */
    private Slider mSeekBarOuter;
    /**
     * The constant M seek bar inner
     */
    private Slider mSeekBarInner;
    /**
     * The constant M seek bar color
     */
    private Slider mSeekBarColor;
    /**
     * The constant M seek bar progress
     */
    private Slider mSeekBarProgress;
    /**
     * The constant M circle progress normal
     */
    private CircleProgressView mCircleProgressNormal;
    /**
     * The constant M circle progress fill in
     */
    private CircleProgressView mCircleProgressFillIn;
    /**
     * The constant M circle progress fill in arc
     */
    private CircleProgressView mCircleProgressFillInArc;
    /**
     * The constant M horizontal progress view
     */
    private HorizontalProgressView mHorizontalProgressView;

    /**
     * On start.
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mCircleProgressNormal = findView(ResourceTable.Id_circle_progress_normal);
        mCircleProgressFillIn = findView(ResourceTable.Id_circle_progress_fill_in);
        mCircleProgressFillInArc = findView(ResourceTable.Id_circle_progress_fill_in_arc);
        mHorizontalProgressView = findView(ResourceTable.Id_horizontal_progress);
        mSeekBarColor = findView(ResourceTable.Id_seek_bar_color);
        mSeekBarInner = findView(ResourceTable.Id_seek_bar_inner);
        mSeekBarOuter = findView(ResourceTable.Id_seek_bar_outer);
        mSeekBarProgress = findView(ResourceTable.Id_seek_bar_progress);
        mSeekBarColor.setValueChangedListener(this);
        mSeekBarInner.setValueChangedListener(this);
        mSeekBarOuter.setValueChangedListener(this);
        mSeekBarProgress.setValueChangedListener(this);
        findComponentById(ResourceTable.Id_btn_gotodemo).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new DemoAbilitySlice(), new Intent());
            }
        });
    }

    /**
     * Adjust alpha .
     *
     * @param color  color
     * @param factor factor
     * @return the int
     */
    private int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = RgbColor.fromArgbInt(color).getRed();
        int green = RgbColor.fromArgbInt(color).getGreen();
        int blue = RgbColor.fromArgbInt(color).getBlue();
        return Color.argb(alpha, red, green, blue);
    }

    /**
     * find view by id.
     *
     * @param <Type> parameter
     * @param id  id
     * @return the t
     */
    private <Type extends Component> Type findView(int id) {
        return (Type) findComponentById(id);
    }


    /**
     * On progress updated.
     *
     * @param slider   slider
     * @param progress progress
     * @param bool        bool
     */
    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean bool) {
        switch (slider.getId()) {
            case ResourceTable.Id_seek_bar_progress:
                mCircleProgressNormal.setProgressValue(progress);
                mCircleProgressFillIn.setProgressValue(progress);
                mCircleProgressFillInArc.setProgressValue(progress);
                mHorizontalProgressView.setProgressValue(progress);
                break;
            case ResourceTable.Id_seek_bar_inner:
                mCircleProgressNormal.setNormalBarSize(progress == 0 ? 1 : progress / 6);
                mCircleProgressFillInArc.setInnerPadding(progress == 0 ? 1 : progress / 6);
                mHorizontalProgressView.setNormalBarSize(progress == 0 ? 1 : progress / 6);
                break;
            case ResourceTable.Id_seek_bar_outer:
                mCircleProgressNormal.setReachBarSize(progress == 0 ? 1 : progress / 6);
                mCircleProgressFillIn.setReachBarSize(progress == 0 ? 1 : progress / 6);
                mHorizontalProgressView.setReachBarSize(progress == 0 ? 1 : progress / 6);
                break;
            case ResourceTable.Id_seek_bar_color:
                int color = Math.abs(new SecureRandom().nextInt(Integer.MAX_VALUE));

                        if(progress <=15){
                            color =  0xff9BCD9B;// int Cyan1 = 0xff00FFFF;
                        } else if(progress >15 && progress <=30){
                            color =  0xff00C5CD;
                        } else if(progress >30 && progress <=45){
                            color =  0xffEECFA1;
                        } else if(progress >45 && progress <=60){
                            color =  0xffB22222;
                        } else if(progress >75 && progress <=90){
                            color =  0xff8B008B;
                        } else if(progress >90 && progress <=100){
                            color =  0xff0000CD;
                        }

               // int color = Math.abs(new SecureRandom().nextInt());
                mCircleProgressNormal.setReachBarColor(color);
                mCircleProgressNormal.setNormalBarColor(adjustAlpha(color, 0.3f));

                mHorizontalProgressView.setReachBarColor(color);
                mHorizontalProgressView.setNormalBarColor(adjustAlpha(color, 0.3f));

                mCircleProgressFillIn.setReachBarColor(color);
                mCircleProgressFillIn.setNormalBarColor(adjustAlpha(color, 0.3f));

                mCircleProgressFillInArc.setReachBarColor(color);
                mCircleProgressFillInArc.setOuterColor(adjustAlpha(color, 0.3f));

                break;
        }
    }

    /**
     * On touch start.
     *
     * @param slider slider
     */
    @Override
    public void onTouchStart(Slider slider) {

    }

    /**
     * On touch end.
     *
     * @param slider slider
     */
    @Override
    public void onTouchEnd(Slider slider) {

    }
}
