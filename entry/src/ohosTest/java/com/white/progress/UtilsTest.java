package com.white.progress;

import com.white.progressview.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testDp2Px() {
        int pxValue = Utils.dp2px(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext(),6);
        assertEquals(18, pxValue);
    }
    @Test
    public void testSp2px() {
        int pxValue = Utils.sp2px(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext(),20);
        assertEquals(60, pxValue);
    }
//    @Test
//    public void testTwo() {
//        //AbilityDelegatorRegistry.getAbilityDelegator().getAppContext(),
//        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
//        assertEquals("com.white.progress", actualBundleName);
//    }
}
